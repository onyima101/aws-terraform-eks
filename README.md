# aws-terraform-eks

Kindly follow this steps to avoid errors.

1, create the s3 and dynamodb by initializing and executing terraform plan on provider.tf and setup.tf 

2, add lock.tf to directory & create the terraform-state lock by initializing and executing terraform plan

3, add cluster.tf to directory & run the terraform init on everyother file in the directory.

terraform init && terraform plan && terraform apply 

To interact with your cluster, run this command in your terminal:

aws eks --region us-east-1 update-kubeconfig --name eks-cluster
